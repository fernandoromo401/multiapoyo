#### DEMO
link del [DEMO](https://multiapoyo.pages.dev/)

#### Credenciales
- user: eve.holt@reqres.in
- password: xxxxxxxx

#### Iniciar proyecto
`npm start` o `yarn start`

#### Desarrollo que falto o pudo mejorarse
- Utilizar enviroments
- Ordenar mejor la estructura del proyecto
- Tipar datos de componentes y funciones (typescript) o al menos el uso de proptypes
- Unit testing y coverage testing
- Modularizar los estilos de styled-components
- Analizar mejor la UI/UX
- Documentar el código
- Remplazar spinner de carga con skeleton de la UI

#### UI - capturas

##### Vista del login

| Escritorio | Movil |
| ------ | ------ |
| ![](./public/mocks/d6.png) | ![](./public/mocks/m1.jpg) |


##### Vista de usuarios

| Escritorio | Movil |
| ------ | ------ |
| ![](./public/mocks/d3.png) | ![](./public/mocks/m3.jpg) |
| ![](./public/mocks/d4.png) | ![](./public/mocks/m4.jpg) |
| ![](./public/mocks/d1.png) | ![](./public/mocks/m6.jpg)

##### Vista de album

| Escritorio | Movil |
| ------ | ------ |
| ![](./public/mocks/d5.png) | ![](./public/mocks/m5.jpg) |
| ![](./public/mocks/d2.png) | ![](./public/mocks/m2.jpg) |




