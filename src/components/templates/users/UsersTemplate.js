import React from 'react'
import TitlePage from '../../common/TitlePage'
import GridCard from '../../common/GridCard'
import CardUser from './components/CardUser'
import useUsers from '../../../hooks/useUsers'
import MoreView from './components/MoreView'
import Loading from '../../common/Loading'
import NoResultsFound from '../../common/NoResult'
import GroupIcon from '@mui/icons-material/Group';

const UsersTemplate = () => {

  const { page, totalPages, list, loading, error } = useUsers()

  if (!!error) {
    return <NoResultsFound/>
  }

  if (!!loading || !list) {
    return <Loading />
  }

  return (
    <div>
      <TitlePage title="Usuarios" icon={GroupIcon} />
      <GridCard cards={list} component={CardUser} width="300px" />
      {(!!list && page < totalPages) && <MoreView/>}
    </div>
  )
}

export default UsersTemplate