import React from 'react'
import AlbumTemplate from '../../components/templates/album/AlbumTemplate'

const Album = () => {
    return (
        <AlbumTemplate />
    )
}

export default Album