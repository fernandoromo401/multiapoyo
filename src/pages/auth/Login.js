import React from 'react'
import LoginTemplate from '../../components/templates/auth/login/LoginTemplate';

const Login = () => {
    return <LoginTemplate/>
}

export default Login
