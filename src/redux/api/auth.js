import axios from "axios";

// const URI = process.env.REACT_APP_RE
const URI = "https://reqres.in"

export function loginUserAPI(payload) {
	return axios.post(`${URI}/api/login`, payload);
}