import * as types from "../types/auth";
import persistReducer from "redux-persist/es/persistReducer";
import storage from "redux-persist/lib/storage";

const initialState = {
	user: null,
	authToken: null,
	loading: false,
	error: null,
};

export const auth = persistReducer(
	{ storage, key: "auth", whitelist: ["user", "authToken"] },
	(state = initialState, action) => {
		switch (action.type) {
			// LOGIN USER
			case types.LOGIN_USER_REQUEST:
				return {
					...state,
					loading: true,
				};
			case types.LOGIN_USER_SUCCESS:
				return {
					...state,
					loading: false,
					authToken: action.payload.token,
					user: action.payload.email
				};
			case types.LOGIN_USER_FAIL:
				return {
					...state,
					loading: false,
					error: action.payload,
				};
			case types.LOGOUT_USER:
				localStorage.clear();
				return initialState
			default:
				return state;
		}
	}
);
