import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { connect } from "react-redux";
import Layout from "../components/layout/Layout";
import Loading from "../components/common/Loading";

const PrivateRoute = (props) => {
    const { component: Component, isAuth } = props;

    const navigate = useNavigate()

    useEffect(() => {
        !isAuth && navigate('/')
    }, [isAuth, navigate])

    if (!isAuth) {
        return <Loading/>
    }    

    return (
        <Layout>
            <Component />
        </Layout>
    );

}

const mapStateToProps = (state) => ({
    isAuth: !!state.auth.authToken,
});

export default connect(mapStateToProps)(PrivateRoute);
