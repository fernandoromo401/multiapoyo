import { createTheme } from '@mui/material/styles';

export const theme = createTheme({
    palette: {
        primary: {
            main: "#5e43c3",
        },
        secondary: {
            main: "#23155B",
        },
    },
    components: {
        // Name of the component
        MuiButton: {
            styleOverrides: {
                root: {
                    fontSize: '14px',
                    textTransform: 'none',
                    borderRadius: '50px',
                    fontWeight: 'bold',
                },
            },
        },
    },
});
